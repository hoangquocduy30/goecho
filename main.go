package main

import (
	"goecho/handler"
	"goecho/mdw"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {

	server := echo.New()

	server.Use(middleware.Logger())

	isLogedIn := middleware.JWT([]byte("secret"))
	isAdmin := mdw.IsAdminMdw

	server.GET("/", handler.Hello, isLogedIn)

	server.POST("/login", handler.Login, middleware.BasicAuth(mdw.BasicAuth))

	server.GET("/admin", handler.Hello, isLogedIn, isAdmin)

	groupv2 := server.Group("v2")

	groupv2.GET("/hello", handler.Hello2)

	groupUser := server.Group("/api/user")

	groupUser.GET("/GetUser", handler.GetUser)

	groupUser.GET("/UpdateUser", handler.UpdateUser)

	groupUser.GET("/DeleteUser", handler.DeleteUser)

	groupUser.GET("/GetAllUser", handler.GetAllUsers)

	server.Logger.Fatal(server.Start(":8888"))
}
