package handler

import (
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
)

func Login(c echo.Context) error {
	username := c.Get("username").(string)
	password := c.Get("password").(string)
	// Throws unauthorized error
	if username != "admin" || password != "123456" {
		return echo.ErrUnauthorized
	}
	// Create token
	token := jwt.New(jwt.SigningMethodHS256)
	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = "admin"
	claims["admin"] = true
	claims["exp"] = time.Now().Add(2 * time.Minute).Unix()

	// Generate encoded token and send it as response.

	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}
