package handler

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
)

func GetUser(c echo.Context) error {

	return c.String(http.StatusOK, "api get user")

}

func UpdateUser(c echo.Context) error {

	return c.String(http.StatusOK, "api update user ")
}

func DeleteUser(c echo.Context) error {

	return c.String(http.StatusOK, "api delete user ")
}

func GetAllUsers(c echo.Context) error {
	c.Response().Header().Set(echo.HeaderContentType, "application/json")
	c.Response().WriteHeader(http.StatusOK)

	enc := json.NewEncoder(c.Response())

	for _, user := range listUsers {

		if err := enc.Encode(user); err != nil {
			return err
		}
		c.Response().Flush()
		time.Sleep(1 * time.Second)
	}
	return nil
}

type User struct {
	Name string
	Age  int
}

var listUsers = []User{
	{Name: "duy", Age: 3},
	{Name: "duy", Age: 3},
	{Name: "duy", Age: 3},
	{Name: "duy", Age: 3},
	{Name: "duy", Age: 3},
	{Name: "duy", Age: 3},
	{Name: "duy", Age: 3},
}
