package mdw

import "github.com/labstack/echo/v4"

func BasicAuth(username string, password string, c echo.Context) (bool, error) {
	if username != "admin" || password != "123456" {
		return false, nil
	}

	c.Set("username", username)
	c.Set("password", password)

	return true, nil
}
